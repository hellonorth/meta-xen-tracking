Simple layer to apply upstream revision tracking to the primary Xen recipes.

License
-------

All metadata is MIT licensed unless otherwise stated.
